DESCRIPTION:

Histograms from FLUKA output files ph88, ph89 ..... (input_usrtrack_88_tab.lis, ....) are transfered into CERN ROOT histograms. Execution of makeROOTfile.py script creates filePhotons.root with histogram for each "tab.lis" file. Then trivial ROOT macro photonPlot.C is used to plot histograms together in one plot.

Following lines should produce histogram h.pdf

 python makeROOTfile.py <br/>
 root -l filePhotons.root <br/>
 .x  photonPlot.C <br/>

Macro photonPlot.C will be replaced by CMS TDR style macro from:

https://twiki.cern.ch/twiki/bin/view/CMS/Internal/FigGuidelines


