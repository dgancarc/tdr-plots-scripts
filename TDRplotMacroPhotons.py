import ROOT as rt
import CMS_lumi, tdrstyle
import array

ROOTfileName = 'photonsROOTfile.root'
pdfFileName = 'TDRplotPhotons.pdf'


myfile = rt.TFile( pdfFileName, 'RECREATE' )


#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "Photons, 3.2 #times 10^{17} pp collision (14 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

yAxisMin = 1e-8
yAxisMax = 9e-5
xAxisMin = 1e-5
xAxisMax = 1

iPos = 11
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 600; 
W_ref = 800; 
W = W_ref
H  = H_ref


iPeriod = 0

T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

canvas = rt.TCanvas("c2","c2",50,50,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)
canvas.cd()
canvas.Update()
canvas.SetLogy(True)
canvas.SetLogx(True)


file = rt.TFile(ROOTfileName,"READ")
hist = rt.TH1F()
hist = file.Get("photons92Int")
xAxis = hist.GetXaxis()
yAxis = hist.GetYaxis()
yAxis.SetRangeUser(yAxisMin,yAxisMax);
xAxis.SetRangeUser(xAxisMin,xAxisMax);
yAxis.SetTitle("E.d#Phi/dE [ cm^{-2}s^{-1} ]");
xAxis.SetTitle("E [ GeV ]");
xAxis.SetTitleOffset(1.44);
yAxis.SetTitleOffset(1.44);
hist2 = file.Get("photons88Int")
hist3 = file.Get("photons89Int")
hist4 = file.Get("photons90Int")
hist5 = file.Get("photons91Int")
hist6 = file.Get("photons93Int")
hist7 = file.Get("photons94Int")
hist.SetLineWidth(3)
hist2.SetLineWidth(3)
hist3.SetLineWidth(3)
hist4.SetLineWidth(3)
hist5.SetLineWidth(3)
hist6.SetLineWidth(3)
hist7.SetLineWidth(3)
hist.SetLineColor(rt.kRed-4)
hist2.SetLineColor(rt.kCyan-4)
hist3.SetLineColor(rt.kOrange+4)
hist4.SetLineColor(rt.kGreen)
hist5.SetLineColor(rt.kViolet-1)
hist6.SetLineColor(rt.kYellow-7)
hist7.SetLineColor(rt.kBlue)

hist.Draw("hist")
hist2.Draw("samehist")
hist3.Draw("samehist")
hist4.Draw("samehist")
hist5.Draw("samehist")
hist6.Draw("samehist")
hist7.Draw("samehist")

le =  rt.TLegend(0.75,0.6,0.82,0.88)
le.SetFillColor(0);
le.SetTextSize(0.04);
le.SetBorderSize(0);
le.AddEntry(hist," RS down","f");
le.AddEntry(hist4," RS upper","f");
le.AddEntry(hist6," HF distant","f");
le.AddEntry(hist3," HF close","f");
le.AddEntry(hist2," BH","f");
le.AddEntry(hist7," Balcony highZ","f");
le.AddEntry(hist5," Balcony lowZ","f");
le.SetTextFont(42)
le.SetTextSize(0.035)    
le.SetTextAlign(12) 
le.Draw("same");
latex = rt.TLatex()
latex.SetNDC()
latex.SetTextSize(0.025)
latex.SetTextColor(33)
latex.SetTextFont(52)
latex.DrawText(0.02,0.02,"CMS FLUKA Simulation v5.0.0.3")


CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)

raw_input("Press Enter to save printed histogram to pdf file")
myfile.Write()
myfile.Close()
canvas.SaveAs(pdfFileName)
