import ROOT as rt
from array import array
import numpy as np

names = ["HF_neutron.txt","HF_electron.txt","HF_photon.txt","HF_proton.txt","HF_pion_minus.txt","HF_pion_plus.txt"]
nameHist = []
norm = 0.5/1e6 # 0.5 for lattice region, 1e6 for size of region 89

for name in names:
	new_nameHist = name.replace(".txt","")
	nameHist.append(new_nameHist)

nameCount = 0
outHistFile = rt.TFile.Open("ROOTfile"+names[0][0:2]+".root" ,"RECREATE")

for nameFile in names:

	MyTree = rt.TTree("MyTree", "MyTree")
	MyTree.ReadFile( nameFile, "min/D:max/D:x/D:errorperc/D")
	binsLower=[]
	binsHigher=[]
	errror=[]
	for e in MyTree: 
		binsLower.append(e.min)
		binsHigher.append(e.max)

	binsLower.append(binsHigher[-1])
	n = len(binsLower)-1
	bins = array('d',binsLower)
	h = rt.TH1F(nameHist[nameCount],"",n,bins)
	hInt = rt.TH1F(nameHist[nameCount] + "Int","",n,bins)
	count = 1
	for e in MyTree:
		h.SetBinContent(count,norm*e.x)
		h.SetBinError(count,norm*(e.errorperc/100)*e.x)
		hInt.SetBinContent(count,norm*e.x*np.sqrt(e.max*e.min))
		hInt.SetBinError(count,norm*(e.errorperc/100)*np.sqrt(e.max*e.min))
		count += 1

	outHistFile.cd()
	h.Write()
	hInt.Write()
	nameCount += 1

outHistFile.Close()
