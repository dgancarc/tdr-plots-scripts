import ROOT as rt
from array import array
import numpy as np

names = ["photons88.txt","photons89.txt","photons90.txt","photons91.txt","photons92.txt","photons93.txt","photons94.txt"]
nameHist = []
norm = [0.5/1e6,0.5/1e6,0.5/1e6,0.5/16e6,0.5/1e6,0.5/1e6,0.5/8e6] # 0.5 for lattice region, 1e6 for region 88, 89, 92, 93 size and 16e6 for region 91 and 8e06 for region 94 (according to region size)

for name in names:
	new_nameHist = name.replace(".txt", "")
	nameHist.append(new_nameHist)



nameCount = 0
outHistFile = rt.TFile.Open("photonsROOTfile.root" ,"RECREATE")

for nameFile in names:
	
	MyTree = rt.TTree("MyTree", "MyTree")
	MyTree.ReadFile( nameFile, "min/D:max/D:x/D:errorperc/D")
	binsLower=[]
	binsHigher=[]
	errror=[]
	for e in MyTree: 
		binsLower.append(e.min)
		binsHigher.append(e.max)

	binsLower.append(binsHigher[-1])
	n = len(binsLower)-1
	bins = array('d',binsLower)
	h = rt.TH1F(nameHist[nameCount],"",n,bins)
	hInt = rt.TH1F(nameHist[nameCount] + "Int","",n,bins)
	count = 1
	for e in MyTree:
		h.SetBinContent(count,norm[nameCount]*e.x)
		h.SetBinError(count,norm[nameCount]*(e.errorperc/100)*e.x)
		hInt.SetBinContent(count,norm[nameCount]*e.x*np.sqrt(e.max*e.min))
		hInt.SetBinError(count,norm[nameCount]*(e.errorperc/100)*np.sqrt(e.max*e.min))
		count += 1

	outHistFile.cd()
	h.Write()
	hInt.Write()
	nameCount += 1

outHistFile.Close()
