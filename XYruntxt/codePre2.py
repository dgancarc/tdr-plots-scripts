#!/usr/bin/env python

import optparse
import os.path
import ROOT as rt
from array import array
import numpy as np
import re


parser = optparse.OptionParser()
parser.add_option( '-a', '--all',  dest = 'filename',
    action = 'store', type = 'string', default = 'filename',
    help = 'name of input FLUKA file' )
( options, args ) = parser.parse_args()

inp = options.filename
inputFileName = options.filename
fileName=[]
i_bin=[]
for i in range(20, 99):
    if os.path.isfile(inputFileName+"_usrtrack_"+str(i)+"_tab.lis"):
        fileName.append(inputFileName+"_usrtrack_"+str(i)+"_tab.lis")
        i_bin.append(i)
        
    
def create_txt_files(FileName,detector):
    """create a text file with detector output"""
    name = detector + FileName[1:18] + ".txt"
    for i in i_bin:
        if str(i) in FileName:
            nameNumber = str(i)
    for fil in fileName:
        name = nameNumber + '_' + detector
        f = open(name, "w")
        a_file = open(fil)
        firstLine = 1 + search_string(FileName, detector)
        lastLine = firstLine + search_number_of_bins(FileName, detector)
        lines_to_read = range(firstLine,lastLine)

        for position, line in enumerate(a_file):
            if position in lines_to_read:
                f.write(line)

    f.close()
    return name

def search_number_of_bins(inputFileName, detectorInputName):
    """Find number of energy bins in detector"""
    stringValue = ""
    n = int(search_string(inputFileName, detectorInputName))
    lines_to_read = range(n,n+1)
    a_file = open(inputFileName)
    for position, line in enumerate(a_file):
        if position in lines_to_read: stringValue = line[34:38]

    return int(stringValue)

def search_string_in_detector(inputFileName, detectorInputName):
    """Search for the given detector name in output file and return line, where scoring in given detector starts"""
    line_number = 0
    line_n = []
    with open(inputFileName, 'r') as read_obj:
        for line in read_obj:
            line_number += 1
            if detectorInputName in line:
                line_n.append(line_number)
    return line_n
    
def search_string(inputFileName, detectorInputName):
    """Search for the given detector name in output file and return line, where scoring in given detector starts"""
    line_number = 0
    with open(inputFileName, 'r') as read_obj:
        for line in read_obj:
            line_number += 1
            if detectorInputName in line:
                line_n = line_number
    return line_n

def search_detector(inputFileName):
    """Find  detectors"""
    stringValue = []
    n = search_string_in_detector(inputFileName, "Detector n")
    # n = int(search_string_in_detector(inputFileName, "Detector n"))
    for nn in n:
        lines_to_read = range(nn-1,nn)
        a_file = open(inputFileName)
        for position, line in enumerate(a_file):
            if position in lines_to_read: stringValue.append(str.strip(line[29:38]))
    return stringValue

data=[]
for final in fileName:
    detector = []
    detector.append(search_detector(final))
    for det in detector:
        for x in range(len(det)):
            data.append(create_txt_files(final,det[x]))
print(data)


nameNum = ["44","45","46","47","48","49","50","51","52","53","54"]
norm = [0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6,0.5/1e6]
outHistFile = rt.TFile.Open(inp+".root" ,"RECREATE")

   # for i in range(len(i_bin)):
   # print("For input file " + input + " output file usrtrack_ :" + str(i_bin[i]) + " normalisation is " + str(norm[i]))
    


for nameFile in data:
	
	
   ind = nameNum.index(nameFile[0:2]) - 1 


   MyTree = rt.TTree("MyTree", "MyTree")
   MyTree.ReadFile( nameFile, "min/D:max/D:x/D:errorperc/D")
   binsLower=[]
   binsHigher=[]
   errror=[]
   for e in MyTree:
       binsLower.append(e.min)
       binsHigher.append(e.max)

   binsLower.append(binsHigher[-1])
   n = len(binsLower)-1
   bins = array('d',binsLower)
   h = rt.TH1F(nameFile,"",n,bins)
   hInt = rt.TH1F(nameFile + "Int","",n,bins)
   count = 1
   for e in MyTree:
       h.SetBinContent(count,norm[ind]*e.x)
       h.SetBinError(count,norm[ind]*(e.errorperc/100)*e.x)
       hInt.SetBinContent(count,norm[ind]*e.x*np.sqrt(e.max*e.min))
       hInt.SetBinError(count,norm[ind]*(e.errorperc/100)*np.sqrt(e.max*e.min))
       count += 1

   outHistFile.cd()
   h.Write()
   hInt.Write()

outHistFile.Close()


#    create_txt_files(final, detector)
