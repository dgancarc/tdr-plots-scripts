import ROOT as rt
import CMS_lumi, tdrstyle
import array

ROOTfileName = "ROOTfileBH.root"
pdfFileName = 'TDRplotBH.pdf'

myfile = rt.TFile( pdfFileName, 'RECREATE' )


#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "BH, 3.2 #times 10^{17} pp collision (14 TeV)" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

yAxisMin = 1e-12
yAxisMax = 1e-4
xAxisMin = 5e-13
xAxisMax = 5

iPos = 12
if( iPos==0 ): CMS_lumi.relPosX = 0.72

H_ref = 600; 
W_ref = 800; 
W = W_ref
H  = H_ref


iPeriod = 0

T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

canvas = rt.TCanvas("c2","c2",50,50,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)
canvas.cd()
canvas.Update()
canvas.SetLogy(True)
canvas.SetLogx(True)


file = rt.TFile(ROOTfileName,"READ")
hist = rt.TH1F()
hist = file.Get("BH_neutronInt")
xAxis = hist.GetXaxis()
yAxis = hist.GetYaxis()
yAxis.SetRangeUser(yAxisMin,yAxisMax);
xAxis.SetRangeUser(xAxisMin,xAxisMax);
yAxis.SetTitle("E.d#Phi/dE [ cm^{-2}s^{-1} ]");
xAxis.SetTitle("E [ GeV ]");
xAxis.SetTitleOffset(1.44);
yAxis.SetTitleOffset(1.44);
hist2 = file.Get("BH_electronInt")
hist3 = file.Get("BH_photonInt")
hist4 = file.Get("BH_protonInt")
hist5 = file.Get("BH_pion_minusInt")
hist6 = file.Get("BH_pion_plusInt")
hist.SetLineWidth(3)
hist2.SetLineWidth(3)
hist3.SetLineWidth(3)
hist4.SetLineWidth(3)
hist5.SetLineWidth(3)
hist6.SetLineWidth(3)
hist.SetLineColor(rt.kRed-4)
hist2.SetLineColor(rt.kCyan-4)
hist3.SetLineColor(rt.kOrange+4)
hist4.SetLineColor(rt.kGreen)
hist5.SetLineColor(rt.kViolet-1)
hist6.SetLineColor(rt.kYellow-7)

hist.Draw("hist")
hist2.Draw("samehist")
hist3.Draw("samehist")
hist4.Draw("samehist")
hist5.Draw("samehist")
hist6.Draw("samehist")

le =  rt.TLegend(0.15,0.6,0.22,0.88)
le.AddEntry(hist," neutrons","f");
le.AddEntry(hist3," photons","f");
le.AddEntry(hist2," e^{+} e^{-}","f");
le.AddEntry(hist4," protons","f");
le.AddEntry(hist5," #pi^{-}","f");
le.AddEntry(hist6," #pi^{+}","f");
le.SetFillColor(0);
le.SetTextSize(0.04);
le.SetBorderSize(0);
le.SetTextFont(42)
le.SetTextSize(0.035)    
le.SetTextAlign(12) 
le.Draw("same");
latex = rt.TLatex()
latex.SetNDC()
latex.SetTextSize(0.025)
latex.SetTextColor(33)
latex.SetTextFont(52)
latex.DrawText(0.02,0.02,"CMS FLUKA Simulation v5.0.0.3")


CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)

raw_input("Press Enter to save printed histogram to pdf file")
myfile.Write()
myfile.Close()
canvas.SaveAs(pdfFileName)
