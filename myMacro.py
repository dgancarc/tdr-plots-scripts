import ROOT as rt
import CMS_lumi, tdrstyle
import array

myfile = rt.TFile( 'TDRplot.pdf', 'RECREATE' )


#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

iPos = 11
if( iPos==0 ): CMS_lumi.relPosX = 0.12

H_ref = 600; 
W_ref = 800; 
W = W_ref
H  = H_ref


iPeriod = 1

T = 0.08*H_ref
B = 0.12*H_ref 
L = 0.12*W_ref
R = 0.04*W_ref

canvas = rt.TCanvas("c2","c2",50,50,W,H)
canvas.SetFillColor(0)
canvas.SetBorderMode(0)
canvas.SetFrameFillStyle(0)
canvas.SetFrameBorderMode(0)
canvas.SetLeftMargin( L/W )
canvas.SetRightMargin( R/W )
canvas.SetTopMargin( T/H )
canvas.SetBottomMargin( B/H )
canvas.SetTickx(0)
canvas.SetTicky(0)
canvas.cd()
canvas.Update()
canvas.SetLogy(True)
canvas.SetLogx(True)


file = rt.TFile("filePhotons.root","READ")
hist = rt.TH1F()
hist = file.Get("ph92Int")
xAxis = hist.GetXaxis()
yAxis = hist.GetYaxis()
yAxis.SetRangeUser(1e-8,9e-5);
xAxis.SetRangeUser(1e-5,1);
yAxis.SetTitle("E.d#Phi/dE [cm^{-2}s^{-1}]");
xAxis.SetTitle("E [GeV]");
xAxis.SetTitleOffset(1.44);
yAxis.SetTitleOffset(1.44);
hist2 = file.Get("ph88Int")
hist3 = file.Get("ph89Int")
hist4 = file.Get("ph90Int")
hist5 = file.Get("ph91Int")
hist6 = file.Get("ph93Int")
hist7 = file.Get("ph94Int")
hist.SetLineWidth(3)
hist2.SetLineWidth(3)
hist3.SetLineWidth(3)
hist4.SetLineWidth(3)
hist5.SetLineWidth(3)
hist6.SetLineWidth(3)
hist7.SetLineWidth(3)
hist.SetLineColor(rt.kRed-4)
hist2.SetLineColor(rt.kCyan-4)
hist3.SetLineColor(rt.kOrange+4)
hist4.SetLineColor(rt.kGreen)
hist5.SetLineColor(rt.kViolet-1)
hist6.SetLineColor(rt.kYellow-7)
hist7.SetLineColor(rt.kBlue)

hist.Draw("hist")
hist2.Draw("samehist")
hist3.Draw("samehist")
hist4.Draw("samehist")
hist5.Draw("samehist")
hist6.Draw("samehist")
hist7.Draw("samehist")

CMS_lumi.CMS_lumi(canvas, iPeriod, iPos)

latex = rt.TLatex()
n_ = 2

x1_l = 0.92
y1_l = 0.60

dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l

legend =  rt.TPad("legend_0","legend_0",x0_l,y0_l,x1_l, y1_l )
legend.Draw()
legend.cd()

myfile.Write()
myfile.Close()

raw_input("Press Enter to end")
